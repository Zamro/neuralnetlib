#pragma once

#include <memory>

#include "../common.hpp"
#include "../Neurons/NeuronI.hpp"

struct LearningAlgorithm {
    NeuronI *neuron;
    unsigned numberOfWeights = 0;
    unsigned numberOfDataPoints = 0;
    std::vector<ArithmeticType> derivatives;

    LearningAlgorithm() = default;
    LearningAlgorithm(NeuronI *neuron);
    virtual ~LearningAlgorithm() {};

    virtual std::unique_ptr<LearningAlgorithm> clone(NeuronI *neuron) const = 0;
    virtual std::string getName() const = 0;
    virtual std::string toString() const = 0;
    virtual ArithmeticType learn(ArithmeticType errorDerivativeOfOutput);
    virtual void finishBatch() = 0;
    void changeWeights(std::vector<ArithmeticType> deltas);
};
