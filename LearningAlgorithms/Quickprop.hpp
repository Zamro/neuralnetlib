#pragma once

#include "LearningAlgorithm.hpp"

struct QuickpropParameters {
    double learningRate; //epsilon
    double mu; //maximumGrowthFactor
    QuickpropParameters(double learningRate = 0.4, double mu = 1.5) : learningRate(learningRate), mu(mu) {}
};

struct Quickprop : LearningAlgorithm {
    using Parameters = QuickpropParameters;
    Parameters p;
    using LearningAlgorithm::neuron;
    using LearningAlgorithm::numberOfWeights;
    using LearningAlgorithm::derivatives;
    std::vector<double> previousDeltas = std::vector<double>(numberOfWeights, 0);
    std::vector<double> previousDerivatives = std::vector<double>(numberOfWeights, 0);

    Quickprop(NeuronI *neuron, const Parameters p = {}) : LearningAlgorithm(neuron), p(p) {}
    Quickprop(const Parameters p = {}) : p(p) {}

    std::unique_ptr<LearningAlgorithm> clone(NeuronI *neuron) const override;
    std::string getName() const override;
    std::string toString() const override;
    void finishBatch() override;

private:
    double gradientDescentTerm(unsigned i);
    double quadraticTerm(unsigned i);
    double getDelta(unsigned i);
};
