#include "Backprop.hpp"

std::unique_ptr<LearningAlgorithm> Backprop::clone(NeuronI *neuron) const {
    return std::move(std::unique_ptr<LearningAlgorithm>(new Backprop(neuron, p)));
}

std::string Backprop::getName() const {
    return "Backprop";
}

std::string Backprop::toString() const {
    return "lr_" + to_string(p.learningRate) + (p.momentum ? ("_mom_" + to_string(p.momentum)) : "");
}

void Backprop::finishBatch() {
    for(unsigned i = 0; i < numberOfWeights; i++) {
        previousDeltas[i] = p.learningRate * derivatives[i] / numberOfDataPoints + p.momentum * previousDeltas[i];
        derivatives[i] = 0;
    }
    numberOfDataPoints = 0;
    changeWeights(previousDeltas);
}
