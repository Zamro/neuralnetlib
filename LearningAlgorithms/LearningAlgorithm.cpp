#include "LearningAlgorithm.hpp"

ArithmeticType LearningAlgorithm::learn(ArithmeticType errorDerivativeOfOutput) {
    ArithmeticType errorDerivativeOfInput = errorDerivativeOfOutput * neuron->activationFunctionDerivative();
    for (unsigned i = 0; i < numberOfWeights; i++)
        derivatives[i] += errorDerivativeOfInput * neuron->inputs[i];
    ++numberOfDataPoints;

    return errorDerivativeOfInput;
}

void LearningAlgorithm::changeWeights(std::vector<ArithmeticType> deltas) {
    for (unsigned i = 0; i < numberOfWeights; i++)
        neuron->weights[i] -= deltas[i];
}

LearningAlgorithm::LearningAlgorithm(NeuronI *neuron) :
        neuron(neuron),
        numberOfWeights(neuron == nullptr ? 0 : neuron->weights.size()),
        derivatives(numberOfWeights, 0) {}
