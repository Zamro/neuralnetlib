#pragma once

#include "LearningAlgorithm.hpp"


template<class LearningAlgorithmImpl>
struct WeightDecayParameters {
    ArithmeticType lambda;//weight decay parameter
    typename LearningAlgorithmImpl::Parameters learningAlgorithmParameters; //learning algorithm used for learning in connection with weight decay
    WeightDecayParameters(ArithmeticType lambda = 1,
                          typename LearningAlgorithmImpl::Parameters learningAlgorithmParameters = {}) : lambda(lambda),
                                                                                                         learningAlgorithmParameters(
                                                                                                                 learningAlgorithmParameters) {}
};

template<class LearningAlgorithmImpl>
struct WeightDecay : LearningAlgorithmImpl {
    using Parameters = WeightDecayParameters<LearningAlgorithmImpl>;
    Parameters p;
    using LearningAlgorithmImpl::neuron;
    using LearningAlgorithmImpl::numberOfWeights;
    using LearningAlgorithmImpl::derivatives;

    WeightDecay(NeuronI *neuron, const Parameters p = {}) : LearningAlgorithmImpl(neuron,
                                                                                  p.learningAlgorithmParameters),
                                                            p(p) {}

    WeightDecay(const Parameters &p = {}) : LearningAlgorithmImpl(p.learningAlgorithmParameters), p(p) {}
    WeightDecay(const WeightDecay &) = default;

    std::unique_ptr<LearningAlgorithm> clone(NeuronI *neuron) const override;
    std::string getName() const override;
    std::string toString() const override;
    ArithmeticType learn(ArithmeticType errorDerivativeOfOutput) override;
};

template<class LearningAlgorithmImpl>
std::unique_ptr<LearningAlgorithm> WeightDecay<LearningAlgorithmImpl>::clone(NeuronI *neuron) const {
    return std::move(std::unique_ptr<LearningAlgorithm>(new WeightDecay<LearningAlgorithmImpl>(neuron, p)));
}

template<class LearningAlgorithmImpl>
std::string WeightDecay<LearningAlgorithmImpl>::getName() const {
    return "WeightDecay_" + LearningAlgorithmImpl::getName();
}

template<class LearningAlgorithmImpl>
std::string WeightDecay<LearningAlgorithmImpl>::toString() const {
    return "wd_" + to_string(p.lambda) + "_" + LearningAlgorithmImpl::toString();
}

template<class LearningAlgorithmImpl>
ArithmeticType WeightDecay<LearningAlgorithmImpl>::learn(ArithmeticType errorDerivativeOfOutput) {
    auto errorDerivativeOfInput = LearningAlgorithmImpl::learn(errorDerivativeOfOutput);
    const std::vector<ArithmeticType> &weights = neuron->getWeights();
    for(unsigned i = 0; i < numberOfWeights; i++)
        derivatives[i] += p.lambda * weights[i];

    return errorDerivativeOfInput;
}
