#include "Quickprop.hpp"

std::unique_ptr<LearningAlgorithm> Quickprop::clone(NeuronI *neuron) const {
    return std::move(std::unique_ptr<LearningAlgorithm>(new Quickprop(neuron, p)));
}

std::string Quickprop::getName() const {
    return "Quickprop";
}

std::string Quickprop::toString() const {
    return "lr_" + to_string(p.learningRate) + (p.mu ? ("_mu_" + to_string(p.mu)) : "");
}

void Quickprop::finishBatch() {
    for(unsigned i = 0; i < numberOfWeights; i++) {
        derivatives[i] /= numberOfDataPoints;
        previousDeltas[i] = getDelta(i);
        previousDerivatives[i] = derivatives[i];
        derivatives[i] = 0;
    }
    numberOfDataPoints = 0;
    changeWeights(previousDeltas);
}

double Quickprop::gradientDescentTerm(unsigned i) {
    return p.learningRate * derivatives[i];
}

double Quickprop::quadraticTerm(unsigned i) {
    return previousDeltas[i] * derivatives[i] / (previousDerivatives[i] - derivatives[i]);
}

double Quickprop::getDelta(unsigned i) {
    if (previousDeltas[i] * derivatives[i] > previousDeltas[i] * p.mu / (p.mu + 1) * previousDerivatives[i])
        return gradientDescentTerm(i) + p.mu * previousDeltas[i];
    if (previousDeltas[i] * derivatives[i] > 0)
        return gradientDescentTerm(i) + quadraticTerm(i);
    if (previousDeltas[i] == 0)
        return gradientDescentTerm(i);
    return quadraticTerm(i);
}
