#pragma once

#include "LearningAlgorithm.hpp"

struct BackpropParameters {
    double learningRate;
    double momentum;

    BackpropParameters(double learningRate = 1, double momentum = 0) : learningRate(learningRate), momentum(momentum) {}
};

struct Backprop : LearningAlgorithm {
    using Parameters = BackpropParameters;
    Parameters p;
    using LearningAlgorithm::neuron;
    using LearningAlgorithm::numberOfWeights;
    using LearningAlgorithm::derivatives;
    std::vector<double> previousDeltas = std::vector<double>(numberOfWeights, 0); //for momentum

    Backprop(NeuronI *neuron, const Parameters p = {}) : LearningAlgorithm(neuron), p(p) {}
    Backprop(const Parameters p = {}) : p(p) {}

    std::unique_ptr<LearningAlgorithm> clone(NeuronI *neuron) const override;
    std::string getName() const override;
    std::string toString() const override;
    void finishBatch() override;
};


