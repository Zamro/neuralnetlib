#pragma once

#include <array>
#include <type_traits>

#include "common.hpp"

class NeuralNetI
{
public:
    virtual ArithmeticType getSumOfSquaredWeights() const = 0;
};