#include "common.hpp"
#include <sstream>

std::string to_string(double param) {
    std::ostringstream oss;
    oss.precision(4);
    oss << param;
    return oss.str();
}

std::vector<ArithmeticType> convertStringToVector(const std::string& str){
    std::vector<ArithmeticType> state;
    state.resize(str.size()/sizeof(ArithmeticType));
    std::copy(str.begin(), str.end(), reinterpret_cast<char*>(&*state.begin()));
    return state;
}
