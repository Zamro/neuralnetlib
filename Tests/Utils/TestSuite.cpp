#include <iostream>
#include "TestSuite.hpp"

bool TestSuite::anyTestFailed = false;

void TestSuite::test( std::vector<std::function<void()>> tests) {
    for(auto test : tests)
    {
        try{
            test();
        } catch(const TestFailed&){
            anyTestFailed = true;
        }
    }
    if(anyTestFailed)
        std::cerr << "Some test has failed!\n";
}

void TestSuite::testAssert(bool val, int line, std::function<void(void)> callback) {
    if(!val){
        std::cerr << "\nFailed! assert at line: " << line << "\n";
        callback();
        throw TestFailed{};
    }
}
