#pragma once
#include <exception>
#include <functional>
#include <vector>
#include <iostream>

#define TEST(testName, testSuite) static void testName(){std::cerr<<#testSuite"::"#testName; testSuite sut; sut._##testName(); std::cerr<<" OK!\n";}  void _##testName()
#define ASSERT(val) {auto valT = val; TestSuite::testAssert(valT, __LINE__, [&](){std::cerr<<#val" = "<<valT<<"\n";});}
#define ASSERT_EQ(lhs, rhs) {auto lhsT = lhs; auto rhsT = rhs; TestSuite::testAssert(lhsT == rhsT, __LINE__, [&](){std::cerr<<#lhs" = "<<lhsT<<" == "#rhs"="<<rhsT<<"\n";});}

struct TestFailed : public std::exception{};

struct TestSuite{
    static void test(std::vector<std::function<void()>> tests);
    static void testAssert(bool val, int line, std::function<void(void)> callback = [](){});
private:
    static bool anyTestFailed;
};
