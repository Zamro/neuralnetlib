#pragma once
#include <array>
#include <cmath>

double distance(double a, double b)
{
    return a<b ? b-a : a-b;
}

template<class T, size_t n>
double distance(const std::array<T, n>& a, const std::array<T, n>& b)
{
    double distance = 0;
    for(unsigned i=0;i<n;i++)
        distance += (b[i]-a[i])*(b[i]-a[i]);

    return sqrt(distance);
}

template<class T>
bool outputIsNear(T &n, double d, double tolerance = 0.1)
{
    return distance(n.getOutput(), d) < tolerance;
}
