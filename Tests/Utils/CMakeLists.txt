project(${PROJECT_NAME}_TEST_UTILS VERSION 1.0 )

set(SOURCES
    TestSuite.cpp
    )

set(HEADERS
    TestSuite.hpp
    TestUtilities.hpp
    )

add_library( ${PROJECT_NAME} SHARED
    ${SOURCES}
    ${HEADERS}
 )

 target_include_directories(${PROJECT_NAME}
     PUBLIC .)

set_target_properties(${PROJECT_NAME} PROPERTIES VERSION ${PROJECT_VERSION})

include(GNUInstallDirs)
install(TARGETS ${PROJECT_NAME}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

install(DIRECTORY . DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/NeuralNetLib/Tests/Utils FILES_MATCHING PATTERN "*.hpp")
