#pragma once

#include <NeuralNetLib/Neurons/SigmoidalNeuron.hpp>
#include <NeuralNetLib/LearningAlgorithms/Quickprop.hpp>
#include <NeuralNetLib/common.hpp>
#include <NeuralNetLib/Tests/Utils/TestUtilities.hpp>
#include <NeuralNetLib/Tests/Utils/TestSuite.hpp>

struct QuickpropTestSuite : public TestSuite
{
private:
    template<std::size_t n>
    SigmoidalNeuron<n-1> createSigmoidalNeuron(std::array<ArithmeticType, n> weights)
    {
        std::vector<ArithmeticType> weightsVector(weights.begin(), weights.end());
        std::vector<ArithmeticType>::const_iterator weightsIt = weightsVector.begin();
        return SigmoidalNeuron<n-1>{weightsIt};
    }

    ArithmeticType weight = -0.2, bias = 0.5;
    ArithmeticType learningRate = 0.1;
    SigmoidalNeuron<1> neuronMock = createSigmoidalNeuron(std::array<ArithmeticType, 2>{weight, bias});
    ArithmeticType mu = 3;
    Quickprop sut{&neuronMock,{learningRate, mu}};
    std::array<ArithmeticType, 1> input {-2};
    ArithmeticType y = 0.710949;                    //s = weight * input + bias = 0.9; y = 1/(1+e^-s)
    ArithmeticType dYdS = 0.20550030734;            //y * ( 1 - y)
    ArithmeticType dEdY = 2 / dYdS;                 //chosen, so that dEdS = 2, implies properOutput z = y - dEdy = -9.52
    ArithmeticType dEdW = -4;                       //dEdS * input
    ArithmeticType dEdB = 2;                        //dEdS * 1
    ArithmeticType deltaW = -0.4;                   //at start previousDelta = 0, so delta = learningRate * dE/dW = 0.1 * -4
    ArithmeticType deltaB = 0.2;                    //at start previousDelta = 0, so delta = learningRate * dE/dBias = 0.1 * 2
    //ArithmeticType newWeight = 0.2, newBias = 0.3;//newX = X - deltaX
    ArithmeticType newY = 0.475020813;              //s = newWeight * input + newBias = -0.1; newY = 1/(1+e^-s) = 0.475020813...
    ArithmeticType newdYdS = 0.24937604;            //newY * ( 1 - newY );

    void firstLearning()
    {
        neuronMock.setInput(input);
        ASSERT(outputIsNear(neuronMock, y, 0.01));
        sut.learn(dEdY);
        ASSERT(distance(sut.previousDerivatives[0], 0) < 0.01);
        ASSERT(distance(sut.previousDerivatives[1], 0) < 0.01);
        ASSERT(distance(sut.derivatives[0], dEdW) < 0.01);
        ASSERT(distance(sut.derivatives[1], dEdB) < 0.01);
        ASSERT(distance(sut.previousDeltas[0], 0) < 0.01);
        ASSERT(distance(sut.previousDeltas[1], 0) < 0.01);

        sut.finishBatch();
        ASSERT(distance(sut.previousDerivatives[0], dEdW) < 0.01);
        ASSERT(distance(sut.previousDerivatives[1], dEdB) < 0.01);
        ASSERT(distance(sut.derivatives[0], 0) < 0.01);
        ASSERT(distance(sut.derivatives[1], 0) < 0.01);
        ASSERT(distance(sut.previousDeltas[0], deltaW) < 0.01);
        ASSERT(distance(sut.previousDeltas[1], deltaB) < 0.01);
    }

    void secondLearning(ArithmeticType dEdS , ArithmeticType newDeltaW, ArithmeticType newDeltaB)
    {
        ArithmeticType newdEdOutput = dEdS / newdYdS; //chosen, so that dEdS is proper;
        ArithmeticType newdEdW = input[0] * dEdS; //dEdS * input
        ArithmeticType newdEdB = dEdS; //dEdS * 1

        neuronMock.setInput(input);
        ASSERT(outputIsNear(neuronMock, newY, 0.01));

        sut.learn(newdEdOutput);
        ASSERT(distance(sut.previousDerivatives[0], dEdW) < 0.01);
        ASSERT(distance(sut.previousDerivatives[1], dEdB) < 0.01);
        ASSERT(distance(sut.derivatives[0], newdEdW) < 0.01);
        ASSERT(distance(sut.derivatives[1], newdEdB) < 0.01);
        ASSERT(distance(sut.previousDeltas[0], deltaW) < 0.01);
        ASSERT(distance(sut.previousDeltas[1], deltaB) < 0.01);

        sut.finishBatch();
        ASSERT(distance(sut.previousDerivatives[0], newdEdW) < 0.01);
        ASSERT(distance(sut.previousDerivatives[1], newdEdB) < 0.01);
        ASSERT(distance(sut.derivatives[0], 0) < 0.01);
        ASSERT(distance(sut.derivatives[1], 0) < 0.01);
        ASSERT(distance(sut.previousDeltas[0], newDeltaW) < 0.01);
        ASSERT(distance(sut.previousDeltas[1], newDeltaB) < 0.01);
    }

public:
    //previousDelta for weight < 0, for bias > 0, so taking two separate branches in The QuickpropAlgorithm.pdf
    //1) derivative of same sign, but not above slope deducted from mi
    // maxSlope dEdW is dEdW * mu/(mu+1) = -4 * 3/4 = -3
    // maxSlope dEdB is dEdB * mu/(mu+1) = 2 * 3/4 = 1.5
    // dEdS = 1 gives newdEdW = -2, newdEdB = 1
    // newDelta = learningRate * newdEdW + delta * newdEdW /(dEdW - newdEdW )
    TEST(QuickpropBehavesCorrectlyWhenDerivativeIsSameSignAsPreviousAndNotAboveMaxSlope, QuickpropTestSuite)
    {
        firstLearning();
        ArithmeticType dEdS = 1;
        ArithmeticType newDeltaW = -0.6;   //newdEdW * learningRate + deltaW * newdEdW / (dEdW - newdEdW))
        ArithmeticType newDeltaB = 0.3;      //newdEdB * learningRate + deltaB * newdEdB / (dEdB - newdEdB))
        secondLearning(dEdS, newDeltaW, newDeltaB);
    }

    //previousDelta for weight < 0, for bias > 0, so taking two separate branches in The QuickpropAlgorithm.pdf
    //2) derivative of same sign and above slope deducted from mi
    // maxSlope dEdW is dEdW * mu/(mu+1) = -4 * 3/4 = -3
    // maxSlope dEdB is dEdB * mu/(mu+1) = 2 * 3/4 = 1.5
    // dEdS = 2 gives newdEdW = -4, newdEdB = 2
    // newDelta = learningRate * newdEdW + delta * mu
    TEST(QuickpropBehavesCorrectlyWhenDerivativeIsSameSignAsPreviousAndAboveMaxSlope, QuickpropTestSuite)
    {
        firstLearning();
        ArithmeticType dEdS = 2;
        ArithmeticType newDeltaW = -1.6;   //newdEdW * learningRate + deltaW * mu
        ArithmeticType newDeltaB = 0.8;      //newdEdB * learningRate + deltaB * mu
        secondLearning(dEdS, newDeltaW, newDeltaB);
    }

    //previousDelta for weight < 0, for bias > 0, so taking two separate branches in The QuickpropAlgorithm.pdf
    //3) derivative of the other sign and below max slope
    // maxSlope dEdW is dEdW * mu/(mu+1) = -4 * 3/4 = -3
    // maxSlope dEdB is dEdB * mu/(mu+1) = 2 * 3/4 = 1.5
    // dEdS = -1 gives newdEdW = 2, newdEdB = -1
    // newDelta = delta * newdEdW/(dEdW - newdEdW)
    TEST(QuickpropBehavesCorrectlyWhenDerivativeIsOtherSignThanPreviousAndNotAboveMaxSlope, QuickpropTestSuite)
    {
        firstLearning();
        ArithmeticType dEdS = -1;
        ArithmeticType newDeltaW = 0.13333;    //deltaW * newdEdW / (dEdW - newdEdW)
        ArithmeticType newDeltaB = -0.0666667;   //deltaB * newdEdB / (dEdB - newdEdB)
        secondLearning(dEdS, newDeltaW, newDeltaB);
    }

    static void test() {
        TestSuite::test({
                &QuickpropBehavesCorrectlyWhenDerivativeIsSameSignAsPreviousAndNotAboveMaxSlope,
                &QuickpropBehavesCorrectlyWhenDerivativeIsSameSignAsPreviousAndAboveMaxSlope,
                &QuickpropBehavesCorrectlyWhenDerivativeIsOtherSignThanPreviousAndNotAboveMaxSlope
            });
    }
};