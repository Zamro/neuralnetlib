#include <iostream>

#include <NeuralNetLib/Neurons/SigmoidalNeuron.hpp>
#include <NeuralNetLib/Neurons/LinearNeuron.hpp>
#include "NeuralNetLib/NeuralNet.hpp"
#include <NeuralNetLib/LearningAlgorithms/Quickprop.hpp>
#include <NeuralNetLib/LearningAlgorithms/WeightDecay.hpp>
#include <NeuralNetLib/Utilities.hpp>

template <class T, class D>
void print(T& net, const D& data)
{
    for(auto dataPoint : data)
    {
        auto out = net.predict(dataPoint.first);
        for(auto a : out)
            std::cout<<a<<" ";
        std::cout<<std::endl;
    }
}

void example_1()
{
    std::cout<<__func__<<std::endl;
    NeuralLayer<LinearNeuron, 3, 4> net1;
    NeuralNet<SigmoidalNeuron, LinearNeuron, 3, 4> net2;
    NeuralNet<SigmoidalNeuron, LinearNeuron, 3, 4, 5, 3> net3;
}

void example_2()
{
    std::cout<<__func__<<std::endl;

    using Net = NeuralNet<SigmoidalNeuron, LinearNeuron, 3, 4, 2>;
    Net::Data data = {{{0.5,  0.5, 0.5}, {1, -1}},
                      {{0.5,  0  , 0.5}, {1,  0}},
                      {{0.5, -0.5, 0.5}, {1,  1}}};
    std::cout<<"Training data:"<<std::endl;
    for(const auto& d: data)
        std::cout<<"input: "<<d.first<<", output:"<<d.second<<std::endl;

    Net net;
    double lambda = 0.05, learningRate = 0.1, mu = 1.5;
    net.setLearningAlgorithm(WeightDecay<Quickprop>({lambda, {learningRate, mu}}));
    using StdErr = StandardError<Net::OutputType>;
    net.setErrorFunction(std::make_unique<WeightDecayError<StdErr>>(StdErr{}, lambda));

    std::cout<<"Prediction before learning:"<<std::endl;
    print(net, data);

    std::cout<<"Learning:"<<std::endl;
    for(int i = 0; i < 10; i++)
        std::cout<<"Error "<<i<<" = "<<net.train(data)<<std::endl;

    std::cout<<"Prediction after learning:"<<std::endl;
    print(net, data);
    std::vector<double> save = net.save();

    std::cout<<"Weights of learned network:"<<std::endl;
    for(auto w : save)
        std::cout<<w<<" ";
    std::cout<<std::endl;
}

int main()
{
    example_1();
    example_2();
    return 0;
}
