#include <cassert>
#include <iostream>

#include <cmath>
#include <numeric>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <array>

#include <NeuralNetLib/Neurons/Neuron.hpp>
#include <NeuralNetLib/Neurons/LinearNeuron.hpp>
#include <NeuralNetLib/Neurons/SigmoidalNeuron.hpp>
#include <NeuralNetLib/NeuralNet.hpp>
#include <NeuralNetLib/LearningAlgorithms/Quickprop.hpp>
#include <NeuralNetLib/LearningAlgorithms/WeightDecay.hpp>
#include "QuickpropTestSuite.hpp"
#include <NeuralNetLib/Utilities.hpp>
#include <NeuralNetLib/Tests/Utils/TestUtilities.hpp>


void assertNeuronImprovesOutput(Neuron<3> &neuron)
{
    for (int i = 1; i < 10; i++)
    {
        neuron.setInput(std::array<double, 3>{1, 0, 0});
        neuron.learn(neuron.getOutput() - 0.3);
        neuron.finishBatch();

        neuron.setInput(std::array<double, 3>{0, 1, 0});
        neuron.learn(neuron.getOutput() - 0.5);
        neuron.finishBatch();

        neuron.setInput(std::array<double, 3>{0, 0, 1});
        neuron.learn(neuron.getOutput() - 0.1);
        neuron.finishBatch();
    }
    neuron.setInput(std::array<double, 3>{1, 0, 0});
//    std::cerr<<"0.3 : "<<neuron.getOutput()<<"\n";
    assert(outputIsNear(neuron, 0.3));

    neuron.setInput(std::array<double, 3>{0, 1, 0});
//    std::cerr<<"0.5 : "<<neuron.getOutput()<<"\n";
    assert(outputIsNear(neuron, 0.5));

    neuron.setInput(std::array<double, 3>{0, 0, 1});
//    std::cerr<<"0.1 : "<<neuron.getOutput()<<"\n";
    assert(outputIsNear(neuron, 0.1));
}

void testLearningLinearNeuron()
{
    std::cerr<<"testLearningLinearNeuron\n";
    LinearNeuron<3> neuron;
    neuron.setLearningAlgorithm(Backprop{{0.5, 0}});
    assertNeuronImprovesOutput(neuron);
    std::cerr<<"     ok\n";
}

void testLearningSigmoidalNeuron()
{
    std::cerr<<"testLearningSigmoidalNeuron\n";
    SigmoidalNeuron<3> neuron;
    neuron.setLearningAlgorithm(Backprop{{10, 0}});
    assertNeuronImprovesOutput(neuron);
    std::cerr<<"     ok\n";
}

void testLayerLearns()
{
    std::cerr<<"testLayerLearns\n";
    NeuralLayer<SigmoidalNeuron, 2, 3> layer;
    std::array<double, 2> input{1, 1};
    std::array<double, 3> properOutput{1, 1, 1};

    auto output = layer.predict(input);
    layer.train(input, properOutput);

    assert(distance(layer.predict(input), properOutput) < distance(output, properOutput));
    std::cerr<<"     ok\n";
}

void testOneLayerNeuralNetLearns()
{
    std::cerr<<"testOneLayerNeuralNetLearns\n";
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron, 2, 3> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 3> properOutput{1, 1, 1};

    auto output = net.predict(input);
    net.train(input,properOutput);

    assert(distance(net.predict(input), properOutput) < distance(output, properOutput));
    std::cerr<<"     ok\n";
}

void testTwoLayersNeuralNetLearnsOneValue()
{
    std::cerr<<"testTwoLayersNeuralNetLearnsOneValue\n";
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{1, 0, 1, 1};

    auto output = net.predict(input);
    net.train(input,properOutput);

    assert(distance(net.predict(input), properOutput) < distance(output, properOutput));
    std::cerr<<"     ok\n";
}

void testTwoLayersNeuralNetLearnsTwoValues()
{
    std::cerr<<"testTwoLayersNeuralNetLearnsTwoValues\n";
    NeuralNet<LinearNeuron, LinearNeuron,  2, 5, 4> net;
    net.setLearningAlgorithm(Backprop{{0.005, 0}});
    std::array<double, 2> firstInput{1,1};
    std::array<double, 4> firstProperOutput{1, 0, 1, 1};
    std::array<double, 2> secondInput{0,1};
    std::array<double, 4> secondProperOutput{1, 0, 0, 1};
    for(int i=0;i<10;i++)
    {
        auto firstOutput = net.predict(firstInput);
        auto secondOutput = net.predict(secondInput);
        net.train(firstInput, firstProperOutput);
        net.train(secondInput, secondProperOutput);

        assert(distance(net.predict(firstInput), firstProperOutput) + distance(net.predict(secondInput), secondProperOutput)
               < distance(firstOutput, firstProperOutput) + distance(secondOutput, secondProperOutput));
    }
    std::cerr<<"     ok\n";
}

void testTwoLayerProbabilisticNeuralNetLearnsAndPreservesSum()
{
    std::cerr<<"testTwoLayerProbabilisticNeuralNetLearns\n";
    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> net;
    net.setLearningAlgorithm(Backprop{{10, 0}});

    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 1, 0, 0};

    auto oldOutput = net.predict(input);
    net.train(input, properOutput);
    auto newOutput = net.predict(input);

    assert(distance(newOutput, properOutput) < distance(oldOutput, properOutput) * 0.99);
    assert(distance(std::accumulate(newOutput.begin(), newOutput.end(), 0.), 1.) < 0.0001);
    std::cerr<<"     ok\n";
}

void testNeuralNetCanSaveStateToVector()
{
    std::cerr<<"testNeuralNetCanSaveStateToVector"<<std::endl;
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};

    net.train(input,properOutput);
    auto state = net.save();

    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> loadedNet;
    loadedNet.load(state.begin());

    assert(loadedNet.predict(input) == net.predict(input));
    std::cerr<<"     ok\n";
}

void testProbabilisticNeuralNetCanSaveStateToVector()
{
    std::cerr<<"testProbabilisticNeuralNetCanSaveStateToVector"<<std::endl;
    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> net;

    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};

    net.train(input,properOutput);
    auto state = net.save();

    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> loadedNet;
    loadedNet.load(state.begin());

    assert(loadedNet.predict(input) == net.predict(input));
    std::cerr<<"     ok\n";
}

void testNeuralNetCanSaveStateToString()
{
    std::cerr<<"testNeuralNetCanSaveStateToString"<<std::endl;
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};
    net.train(input, properOutput);
    std::string state = net.saveToString();

    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> loadedNet;
    loadedNet.load(state);

    assert(loadedNet.predict(input) == net.predict(input));
    std::cerr<<"     ok\n";
}

void testNeuralLayerCanSaveStateToString()
{
    std::cerr<<"testNeuralLayerCanSaveStateToString"<<std::endl;
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron, 2, 4> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};
    net.train(input, properOutput);
    std::string state = net.saveToString();

    NeuralNet<SigmoidalNeuron, SigmoidalNeuron, 2, 4> loadedNet;
    loadedNet.load(state);

    assert(loadedNet.predict(input) == net.predict(input));
    std::cerr<<"     ok\n";
}

void testProbabilisticNeuralNetCanSaveStateToString()
{
    std::cerr<<"testProbabilisticNeuralNetCanSaveStateToString"<<std::endl;
    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};
    net.train(input, properOutput);
    std::string state = net.saveToString();

    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> loadedNet;
    loadedNet.load(state);

    assert(loadedNet.predict(input) == net.predict(input));
    std::cerr<<"     ok\n";
}

void createFilesForTestNeuralNetDoesNotChangeBehavior()
{
    std::cerr<<"createFilesForTestNeuralNetDoesNotChangeBehavior"<<std::endl;

    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> net;
    saveNet(net, "notLearnedNet.net");
    std::string loadedState = loadNet("notLearnedNet.net");
    assert(loadedState == net.saveToString());

    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};
    net.train(input, properOutput);

    saveNet(net, "learnedNet.net");

    std::string properState = loadNet("learnedNet.net");
    assert(properState == net.saveToString());

    std::cerr<<"     ok\n";
}

//sprawdzamy, czy ładując stan sieci, po jej uczeniu otrzymujemy taki sam stan,
//jak wtedy, gdy tworzyliśmy ten plik
//w celu uruchomienia tego testu należy najpierw stworzyć odpowiednie pliki przy użyciu funkcji
//createFilesForTestNeuralNetDoesNotChangeBehavior()
void testNeuralNetDoesNotChangeBehavior()
{
    std::cerr<<"testNeuralNetDoesNotChangeBehavior"<<std::endl;

    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> net(loadNet("notLearnedNet.net"));

    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};
    net.train(input, properOutput);

    std::string properState = loadNet("learnedNet.net");
    assert(properState == net.saveToString());

    std::cerr<<"     ok\n";
}


void createFilesForTestProbabilisticNeuralNetDoesNotChangeBehavior()
{
    std::cerr<<"createFilesForTestProbabilisticNeuralNetDoesNotChangeBehavior"<<std::endl;

    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> net;
    saveNet(net, "notLearnedProbNet.net");
    std::string loadedState = loadNet("notLearnedProbNet.net");
    assert(loadedState == net.saveToString());

    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};
    net.train(input, properOutput);

    saveNet(net, "learnedProbNet.net");

    std::string properState = loadNet("learnedProbNet.net");
    auto output = net.predict(input);
    assert(distance(std::accumulate(output.begin(), output.end(), 0.), 1.) < 0.0001);
    assert(properState == net.saveToString());

    std::cerr<<"     ok\n";
}

//sprawdzamy, czy ładując stan sieci, po jej uczeniu otrzymujemy taki sam stan,
//jak wtedy, gdy tworzyliśmy ten plik
//w celu uruchomienia tego testu należy najpierw stworzyć odpowiednie pliki przy użyciu funkcji
//createFilesForTestNeuralNetDoesNotChangeBehavior()
void testProbabilisticNeuralNetDoesNotChangeBehavior()
{
    std::cerr<<"testProbabilisticNeuralNetDoesNotChangeBehavior"<<std::endl;

    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> net(loadNet("notLearnedProbNet.net"));

    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};
    net.train(input, properOutput);

    std::string properState = loadNet("learnedProbNet.net");

    auto output = net.predict(input);
    assert(distance(std::accumulate(output.begin(), output.end(), 0.), 1.) < 0.0001);

    assert(properState == net.saveToString());

    std::cerr<<"     ok\n";
}

void testNetCanChangeErrorFunction()
{
    std::cerr<<"testNetCanChangeErrorFunction"<<std::endl;
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> net;
    typedef std::array<ArithmeticType, 4> OutputType;
    net.setErrorFunction(std::unique_ptr<ErrorFunction<OutputType>>(new StandardError<std::array<ArithmeticType, 4>>()));
    std::vector<OutputType> deviations = {
        {10, 1, 0.5, 0},
        {20, 1, 0.5, 2}
    };
    net.setErrorFunction(std::unique_ptr<ErrorFunction<OutputType>>(new ChiSquaredError<std::array<ArithmeticType, 4>>(deviations)));
    std::cerr<<"     ok\n";
}

void testWeightDecay()
{
    std::cerr<<"testWeightDecay"<<std::endl;
    using Net = NeuralNet<SigmoidalNeuron, LinearNeuron, 2, 1>;
    using StdErr = StandardError<Net::OutputType>;
    ArithmeticType lastVectorLength = std::numeric_limits<double>::max();
    for(double lambda = 0; lambda <= 2;lambda += 0.2)
    {
        Net net;
        Net::Data data = {{{0.5,  0.5}, { 1}},
                          {{0.5,  0  }, { 0}},
                          {{0.5, -0.5}, {-1}}};
        double learningRate = 1, mu = 1.5;
        net.setLearningAlgorithm(WeightDecay<Quickprop>({lambda, {learningRate, mu}}));
        net.setErrorFunction(std::make_unique<WeightDecayError<StdErr>>(StdErr{}, lambda));
        for(int i = 0; i < 100; i++) net.train(data);
        std::vector<double> save = net.save();
        auto weightVectorLength = std::sqrt(std::accumulate(save.begin(), save.end(), 0., [](double a, double b){return a+b*b;}));
        assert(weightVectorLength < lastVectorLength);
        lastVectorLength = weightVectorLength;
    }
    std::cerr<<"     ok\n";
}

int main()
{
    testLearningLinearNeuron();
    testLearningSigmoidalNeuron();
    testLayerLearns();
    testOneLayerNeuralNetLearns();
    testTwoLayersNeuralNetLearnsOneValue();
    testTwoLayersNeuralNetLearnsTwoValues();
    testTwoLayerProbabilisticNeuralNetLearnsAndPreservesSum();
    testNeuralNetCanSaveStateToVector();
    testProbabilisticNeuralNetCanSaveStateToVector();
    testNeuralNetCanSaveStateToString();
    testProbabilisticNeuralNetCanSaveStateToString();
    testNeuralLayerCanSaveStateToString();
//        createFilesForTestNeuralNetDoesNotChangeBehavior();
    testNeuralNetDoesNotChangeBehavior();
//        createFilesForTestProbabilisticNeuralNetDoesNotChangeBehavior();
    testProbabilisticNeuralNetDoesNotChangeBehavior();

    testNetCanChangeErrorFunction();
    testWeightDecay();
    std::cerr<<"All tests passed\n";
    QuickpropTestSuite::test();
}
