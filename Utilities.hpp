#pragma once

#include <cassert>
#include <iostream>
#include <cmath>
#include <numeric>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <array>
#include "LearningAlgorithms/Backprop.hpp"
#include "ErrorFunctions.hpp"
#include "Neurons/Neuron.hpp"
#include "ChiSquaredNetData.hpp"

struct Range
{
    ArithmeticType begin;
    ArithmeticType end;
    ArithmeticType delta;
};

template<std::size_t n>
std::ostream& operator<<(std::ostream& out, std::array<ArithmeticType, n> point)
{
    for(unsigned i = 0; i < n; ++i)
        out<<point[i]<<(i == n - 1 ? "" : " ");
    return out;
}

template<class Net>
void saveNet(const Net& net, const std::string& filename){
    std::string state = net.saveToString();
    std::ofstream out(filename, std::ios_base::binary);
    out<<state;
    out.close();
}

std::string loadNet(const std::string& filename);

struct StartingPoint{
    std::vector<double> startingPoint;
    unsigned epochs = 0;
};

struct NetSaveWithStartingPoint{
    StartingPoint startingPoint;
    double validationError = std::numeric_limits<double>::max();
    std::vector<double> save = {};
    bool operator<(const NetSaveWithStartingPoint& other){
        return validationError < other.validationError;
    }
};

template<class Net, class LearningData>
NetSaveWithStartingPoint trainNetEarlyStopping(
        const LearningData& learningData,
        unsigned maxEpochs,
        unsigned numberOfTries,
        const LearningAlgorithm& learningAlgorithm,
        const ErrorFunction<typename Net::OutputType>& learningErrorFunction,
        const typename Net::Data& validationData,
        ErrorFunction<typename Net::OutputType>& validationErrorFunction,
        unsigned maxEpochsValidationErrorIsNotFalling,
        Net net = {},
        unsigned batchSize = std::numeric_limits<unsigned>::max())
{
    auto startingPoint = net.save();
    std::vector<ArithmeticType> bestPoint = startingPoint;
    net.setLearningAlgorithm(learningAlgorithm);
    net.setErrorFunction(learningErrorFunction.clone());
    validationErrorFunction.setNeuralNet(&net);

    unsigned epochsToLowestValidationError = 0;
    double validationError = std::numeric_limits<double>::max();
    if(not validationData.empty())
        validationError = validationErrorFunction.error(net, validationData);
    double lowestValidationError = validationError;

    for(unsigned epoch = 0;
        epoch < maxEpochs
        && epoch - epochsToLowestValidationError < maxEpochsValidationErrorIsNotFalling;
        epoch++)
    {
        auto begin = learningData.begin();
        validationError = 0;
        while(begin != learningData.end())
        {
            auto step = std::min(unsigned(learningData.end() - begin), batchSize);
            auto end = begin + step;
            validationError += net.train(begin, end);
            begin = end;
        }
        if(not validationData.empty())
            validationError = validationErrorFunction.error(net, validationData);

        if(validationError < lowestValidationError) {
            lowestValidationError = validationError;
            epochsToLowestValidationError = epoch + 1;
            bestPoint = net.save();
        }
    }
    return {{std::move(startingPoint), std::move(epochsToLowestValidationError)}, std::move(lowestValidationError), std::move(bestPoint)};
}

template<class Net, class LearningData>
std::vector<NetSaveWithStartingPoint> trainRandomNets(
        const LearningData& learningData,
        unsigned maxEpochs = 100,
        unsigned numberOfTries = 100,
        const LearningAlgorithm& learningAlgorithm = Backprop{},
        const ErrorFunction<typename Net::OutputType>& learningErrorFunction = StandardError<typename Net::OutputType>{},
        const typename Net::Data& validationData = {},
        const ErrorFunction<typename Net::OutputType>& validationErrorFunctionTemp = StandardError<typename Net::OutputType>{},
        unsigned maxEpochsValidationErrorIsNotFalling = std::numeric_limits<unsigned>::max(),
        unsigned batchSize = std::numeric_limits<unsigned>::max())
{
    std::vector<NetSaveWithStartingPoint> out{};
    auto validationErrorFunction = validationErrorFunctionTemp.clone();
    for(unsigned j = 0; j<numberOfTries; j++)
    {
        out.push_back(trainNetEarlyStopping<Net>(learningData,
                                                 maxEpochs,
                                                 numberOfTries,
                                                 learningAlgorithm,
                                                 learningErrorFunction,
                                                 validationData,
                                                 *validationErrorFunction,
                                                 maxEpochsValidationErrorIsNotFalling,
                                                 {},
                                                 batchSize));
    }
    return out;
}

template<class Net, class ErrorType = ArithmeticType>
struct NetWithLearningErrors{
    Net net;
    std::vector<ErrorType> errors;
    void saveToFiles(const std::string& errorFile,
                     const std::string& outFile,
                     Range range)
    {
        std::ofstream errorOut(errorFile);
        for(unsigned epoch = 0; epoch < errors.size(); ++epoch) {
            errorOut << epoch << " " << errors[epoch] << std::endl;
        }
        errorOut.close();

        std::ofstream out(outFile);
        for(const auto& point : generateOutput(net, range))
            out<<point.first[0]<<" "<<point.second[0]<<std::endl;
        out.close();
    }
    void saveNet(const std::string& filename)
    {
        ::saveNet(net, filename);
    }
};

template<class Net>
NetWithLearningErrors<Net> learnNetAndGetLearningErrors(
        StartingPoint& netStartingPoint,
        const typename Net::Data& data,
        const LearningAlgorithm& learningAlgorithm = Backprop{},
        const ErrorFunction<typename Net::OutputType>& errorFunction = StandardError<typename Net::OutputType>{})
{
    Net net( netStartingPoint.startingPoint.begin() );
    net.setLearningAlgorithm(learningAlgorithm);
    net.setErrorFunction(errorFunction.clone());

    std::vector<ArithmeticType> errors;

    for(unsigned epoch = 0; epoch < netStartingPoint.epochs; epoch++) {
        errors.push_back(net.train(data));
    }
    return {std::move(net), std::move(errors)};
}

template<class Net>
NetWithLearningErrors<Net> getBestNetWithLearningErrors(
        const typename Net::Data& data,
        unsigned epochs = 100,
        unsigned numberOfTries = 100,
        const LearningAlgorithm& learningAlgorithm = Backprop{},
        const ErrorFunction<typename Net::OutputType>& errorFunction = StandardError<typename Net::OutputType>{})
{
    auto randomNets = trainRandomNets<Net>(data, epochs, numberOfTries, learningAlgorithm, errorFunction);
    auto netStartingPoint = std::min(randomNets.begin(), randomNets.end());
    return learnNetAndGetLearningErrors<Net>(netStartingPoint->startingPoint, data, learningAlgorithm, errorFunction);
}

std::ostream& operator<<(std::ostream& out, std::pair<ArithmeticType, ArithmeticType> errors);

template<class Net>
NetWithLearningErrors<Net, std::pair<ArithmeticType, ArithmeticType>> learnNetAndGetLearningErrors(
        const StartingPoint& netStartingPoint,
        const typename Net::Data& learningData,
        const typename Net::Data& validationData,
        const LearningAlgorithm &learningAlgorithm,
        const ErrorFunction<typename Net::OutputType>& learningErrorFunction,
        ErrorFunction<typename Net::OutputType>& validationErrorFunction)
{
    Net net( netStartingPoint.startingPoint.begin() );
    net.setLearningAlgorithm(learningAlgorithm);
    net.setErrorFunction(learningErrorFunction.clone());
    validationErrorFunction.setNeuralNet(&net);

    std::vector<std::pair<ArithmeticType, ArithmeticType>> errors;

    for(unsigned epoch = 0; epoch < netStartingPoint.epochs; epoch++) {
        auto validationError = validationErrorFunction.error(net, validationData);
        auto learningError = net.train(learningData);
        errors.push_back({learningError, validationError});
    }
    auto save = net.save();
    errors.push_back({0.1, 0.1});
    for(unsigned epoch = 0; epoch < 101; epoch++) { //the best point and 100 additional points
        auto validationError = validationErrorFunction.error(net, validationData);
        auto learningError = net.train(learningData);
        errors.push_back({learningError, validationError});
    }
    net.load(save.begin());
    return {std::move(net), std::move(errors)};
}

template<class Net>
typename Net::Data generateOutput(Net& net,
                                  Range range)
{
    typename Net::Data data;
    for(auto x = range.begin; x <= range.end; x += range.delta)
        data.push_back({{x}, net.predict({x})});
    return data;
}
