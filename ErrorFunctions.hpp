#pragma once

#include <vector>
#include <memory>
#include "common.hpp"
#include "NeuralNetI.hpp"

template<class T>
struct ErrorFunction{
    typedef T OutputType;

    virtual std::unique_ptr<ErrorFunction<T>> clone() const = 0;
    virtual ArithmeticType error(const T& netOutput, const T& properOutput) = 0;
    virtual T errorDerivative(const T& netOutput, const T& properOutput) = 0;
    virtual void setNeuralNet(NeuralNetI*){}
    template<class Net>
    ArithmeticType error(Net& net, const typename Net::Data& data)
    {
        ArithmeticType out = 0;
        for(auto& record : data)
        {
            out += this->error(net.predict(record.first), record.second);
        }
        if(data.size() > 0)
            out /= data.size();
        return out;
    }
    virtual std::string getName() const = 0;
    virtual ~ErrorFunction(){}
};

template<class T>
struct StandardError: public ErrorFunction<T>{
    typedef T OutputType;

    std::unique_ptr<ErrorFunction<T>> clone() const override {
        return std::move(std::unique_ptr<ErrorFunction<T>>(new StandardError<T>(*this)));
    }

    ArithmeticType error(const T& netOutput, const T& properOutput) override {
        ArithmeticType  error{};
        for(unsigned i=0; i<netOutput.size(); i++)
            error += (netOutput[i] - properOutput[i])*(netOutput[i] - properOutput[i]);
        return error;
    }

    T errorDerivative(const T& netOutput, const T& properOutput) override {
        T errorDerivatives;
        for(unsigned i=0; i<netOutput.size(); i++)
            errorDerivatives[i] = 2*(netOutput[i] - properOutput[i]);
        return errorDerivatives;
    }

    std::string getName() const override{
        return "Standard Error";
    }
};

template<class T>
struct ChiSquaredError: public ErrorFunction<T>{
    typedef T OutputType;
private:
    std::vector<T>  squaredDeviations;
    unsigned errorIndex = 0;
    unsigned errorDerivativeIndex = 0;
public:
    std::unique_ptr<ErrorFunction<T>> clone() const override{
        return std::move(std::unique_ptr<ErrorFunction<T>>(new ChiSquaredError<T>(*this)));
    }

    ChiSquaredError(const ChiSquaredError&) = default;

    explicit ChiSquaredError(const std::vector<T>& deviations)
    {
        squaredDeviations.reserve(deviations.size());
        for(auto deviationsArray:deviations)
        {
            squaredDeviations.push_back({});
            for(unsigned i = 0; i < deviationsArray.size(); i++)
                squaredDeviations.back()[i] = deviationsArray[i]*deviationsArray[i];
        }
    }

    ArithmeticType error(const T& netOutput, const T& properOutput) override {
        ArithmeticType  error{};
        for(unsigned i=0;i<netOutput.size();i++)
            error += (netOutput[i] - properOutput[i])*(netOutput[i] - properOutput[i])/squaredDeviations[errorIndex][i];
        errorIndex = (errorIndex + 1) % squaredDeviations.size();
        return error;
    }

    T errorDerivative(const T& netOutput, const T& properOutput) override {
        T errorDerivatives;
        for(unsigned i=0;i<netOutput.size();i++)
            errorDerivatives[i] = 2*(netOutput[i] - properOutput[i])/squaredDeviations[errorDerivativeIndex][i];
        errorDerivativeIndex = (errorDerivativeIndex + 1) % squaredDeviations.size();
        return errorDerivatives;
    }

    std::string getName() const override{
        return "Chi Squared Error";
    }
};

template<class ErrorFunctionImpl>
struct WeightDecayError: public ErrorFunctionImpl{
    typedef typename ErrorFunctionImpl::OutputType T;
    ArithmeticType lambda;
    NeuralNetI* net;

    std::unique_ptr<ErrorFunction<T>> clone() const override{
        return std::move(std::unique_ptr<ErrorFunction<T>>(new WeightDecayError<ErrorFunctionImpl>(*this)));
    }

    explicit WeightDecayError(const ErrorFunctionImpl& error, ArithmeticType lambda = 1):
            ErrorFunctionImpl(error),
            lambda(lambda)
    {}

    WeightDecayError(const WeightDecayError&) = default;

    ArithmeticType error(const T& netOutput, const T& properOutput) override {
        return ErrorFunctionImpl::error(netOutput, properOutput) + lambda / 2 * net->getSumOfSquaredWeights();
    }

    void setNeuralNet(NeuralNetI* net) override {
        this->net = net;
    }

    std::string getName() const override{
        return ErrorFunctionImpl::getName() + " with weight decay";
    }
};
