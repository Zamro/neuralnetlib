#pragma once
#include <string>
#include <vector>

typedef double ArithmeticType;

std::string to_string(double param);
std::vector<ArithmeticType> convertStringToVector(const std::string& str);
