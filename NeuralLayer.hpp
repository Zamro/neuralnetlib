#pragma once
#include "NeuralLayerImpl.hpp"
#include "Neurons/SoftmaxNeuron.hpp"

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
class NeuralLayer : public NeuralLayerImpl<NeuronType, inputs, nodesNumber>
{
    using NeuralLayerImpl<NeuronType, inputs, nodesNumber>::NeuralLayerImpl;
};

template <unsigned inputs, unsigned nodesNumber>
class NeuralLayer<SoftmaxNeuron, inputs, nodesNumber> : public NeuralLayerImpl<SoftmaxNeuron, inputs, nodesNumber>
{
    using NeuralLayerImpl<SoftmaxNeuron, inputs, nodesNumber>::NeuralLayerImpl;
    void prepareOutput() override{
        ArithmeticType exponentsSum = 0;
        for(const auto& neuronPtr : this->neurons)
            exponentsSum += neuronPtr->getExponent();
        for(const auto& neuronPtr : this->neurons)
            neuronPtr->setExponentSum(exponentsSum);
    }
};
