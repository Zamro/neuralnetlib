#pragma once

#include <array>
#include <type_traits>
#include <functional>

#include "Neurons/Neuron.hpp"
#include "NeuralLayer.hpp"

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
class NeuralNet : public NeuralNetI
{
    template<template<unsigned> class, template<unsigned> class, unsigned, unsigned, unsigned...>
    friend class NeuralNet;
    static_assert(inputs > 0, "All layers have to be non empty");
    NeuralLayer<HiddenNeuronType, inputs, nodes> layer;
    NeuralNet<HiddenNeuronType, OutputNeuronType, nodes, nextLayers...> net;
public:
    typedef decltype(net.getOutput()) OutputType;
    OutputType getOutput();
    typedef std::array<ArithmeticType, inputs> InputType;
    typedef std::vector<std::pair<InputType, OutputType>> Data;
private:
    void learn(const OutputType& errorDerivativesOfNetworkOutput);
    InputType learnAndGetPreviousLayerDerivatives(const OutputType& errorDerivativesOfNetworkOutputs);
    void setInput(const InputType& input);
    void finishBatch();

    std::unique_ptr<ErrorFunction<OutputType>> errorFunction{new StandardError<OutputType>()};
public:
    NeuralNet() = default;
    NeuralNet(const std::string& str);
    NeuralNet(std::vector<ArithmeticType>::const_iterator state);

    OutputType predict(const InputType& input);
    ArithmeticType train(const InputType& input, OutputType feedback);
    ArithmeticType train(const Data& data);
    template<class Iterator>
    ArithmeticType train(Iterator begin, const Iterator& end);

    std::vector<ArithmeticType> save() const;
    std::string saveToString() const;
    std::vector<ArithmeticType>::const_iterator load(std::vector<ArithmeticType>::const_iterator state);
    void load(const std::string& str);

    void setLearningAlgorithm(const LearningAlgorithm& learningAlgorithm);
    void setErrorFunction(std::unique_ptr<ErrorFunction<OutputType>>&& errorFunction);
    ArithmeticType getSumOfSquaredWeights() const override;
};

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes>
class NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes> :
        public NeuralLayer<OutputNeuronType, inputs, nodes>
{
public:
    using NeuralLayer<OutputNeuronType, inputs, nodes>::NeuralLayer;
};

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
void NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    learn(const OutputType& errorDerivativesOfNetworkOutput)
{
    auto errorDerivativesOfLayerOutputs = net.learnAndGetPreviousLayerDerivatives(errorDerivativesOfNetworkOutput);
    layer.learn(errorDerivativesOfLayerOutputs);
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
auto NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    learnAndGetPreviousLayerDerivatives(const OutputType& errorDerivativesOfNetworkOutputs) -> InputType
{
    auto errorDerivativesOfLayerOutputs = net.learnAndGetPreviousLayerDerivatives(errorDerivativesOfNetworkOutputs);
    return layer.learnAndGetPreviousLayerDerivatives(errorDerivativesOfLayerOutputs);
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
void NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    setInput(const std::array<ArithmeticType, inputs>& input)
{
    net.setInput(layer.predict(input));
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
auto NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    getOutput() -> OutputType
{
    return net.getOutput();
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
void NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    finishBatch()
{
    layer.finishBatch();
    net.finishBatch();
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    NeuralNet(const std::string& str):
        NeuralNet(convertStringToVector(str).begin())
{
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    NeuralNet(std::vector<ArithmeticType>::const_iterator state):
        layer(state),
        net(state)
{
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
auto NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    predict(const InputType& input) -> OutputType
{
    setInput(input);
    return getOutput();
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
ArithmeticType NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    train(const InputType& input, OutputType feedback)
{
    return train({{input, feedback}});
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
ArithmeticType NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    train(const std::vector<std::pair<InputType, OutputType> >& data)
{
    return train(data.begin(), data.end());
}


template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
template<class Iterator>
ArithmeticType NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    train(Iterator begin, const Iterator& end)
{
    ArithmeticType errorOut{};
    unsigned count = 0;
    while(begin != end)
    {
        auto output = predict(begin->first);
        errorOut += errorFunction->error(output, begin->second);
        learn(errorFunction->errorDerivative(output, begin->second));
        ++begin;
        ++count;
    }
    finishBatch();
    return errorOut/count;
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
std::vector<ArithmeticType> NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    save() const
{
    auto out = layer.save();
    auto netSave = net.save();
    out.reserve( out.size() + netSave.size() );
    out.insert( out.end(), netSave.begin(), netSave.end() );
    return out;
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
std::string NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    saveToString() const
{
    auto out = save();
    return std::string(reinterpret_cast<char*>(&*out.begin()), out.size()*sizeof(ArithmeticType));
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
std::vector<ArithmeticType>::const_iterator NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    load(std::vector<ArithmeticType>::const_iterator state)
{
    return net.load(layer.load(state));
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
void NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    load(const std::string& str)
{
    auto state = convertStringToVector(str);
    load(state.begin());
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
void NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    setLearningAlgorithm(const LearningAlgorithm& learningAlgorithm)
{
    layer.setLearningAlgorithm(learningAlgorithm);
    net.setLearningAlgorithm(learningAlgorithm);
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
void NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    setErrorFunction(std::unique_ptr<ErrorFunction<OutputType>>&& errorFunction)
{
    this->errorFunction = std::move(errorFunction);
    this->errorFunction->setNeuralNet(this);
}

template<template<unsigned> class HiddenNeuronType, template<unsigned> class OutputNeuronType, unsigned inputs, unsigned nodes, unsigned... nextLayers>
ArithmeticType NeuralNet<HiddenNeuronType, OutputNeuronType, inputs, nodes, nextLayers...>::
    getSumOfSquaredWeights() const
{
    return layer.getSumOfSquaredWeights() + net.getSumOfSquaredWeights();
}
