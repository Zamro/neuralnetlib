#pragma once

#include <cmath>

#include "BiasedNeuron.hpp"

template<unsigned n>
class SigmoidalNeuron : public BiasedNeuron<n>
{
public:
    using BiasedNeuron<n>::BiasedNeuron;
protected:
    virtual ArithmeticType activationFunction() const override;
    virtual ArithmeticType activationFunctionDerivative() const override;
};

template<unsigned n>
ArithmeticType SigmoidalNeuron<n>::activationFunction() const
{
    return 1. / (1. + exp( -Neuron<n>::s));
}

template<unsigned n>
ArithmeticType SigmoidalNeuron<n>::activationFunctionDerivative() const
{
    auto activation = activationFunction();
    return activation * (1 - activation);
}