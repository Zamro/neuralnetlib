#include "NeuronI.hpp"
#include "../LearningAlgorithms/LearningAlgorithm.hpp"

#include <numeric>
const std::vector<ArithmeticType>& NeuronI::getWeights() const
{
    return weights;
}

ArithmeticType NeuronI::getSumOfSquaredWeights()
{
    return std::accumulate(weights.begin(), weights.end(), 0, [](ArithmeticType a, ArithmeticType b){return a + b*b;});
}

std::vector<ArithmeticType> NeuronI::save() const
{
    std::vector<ArithmeticType> out;
    out.insert(out.end(), weights.begin(), weights.end());
    return out;
}

void NeuronI::setLearningAlgorithm(const LearningAlgorithm& learningAlgorithm)
{
    this->learningAlgorithm = learningAlgorithm.clone(this);
}

ArithmeticType NeuronI::getOutput()
{
    return activationFunction();
}

ArithmeticType NeuronI::learn(ArithmeticType errorDerivativeOfOutput)
{
    return learningAlgorithm->learn(errorDerivativeOfOutput);
}

void NeuronI::finishBatch()
{
    learningAlgorithm->finishBatch();
}
