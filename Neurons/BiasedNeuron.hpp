#pragma once

#include "Neuron.hpp"

template<unsigned n>
class BiasedNeuron : public Neuron<n>
{
public:
    BiasedNeuron();
    BiasedNeuron(std::vector<ArithmeticType>::const_iterator& state);
    virtual std::vector<ArithmeticType>::const_iterator load(std::vector<ArithmeticType>::const_iterator state) override;
};

template<unsigned n>
std::vector<ArithmeticType>::const_iterator BiasedNeuron<n>::load(std::vector<ArithmeticType>::const_iterator state)
{
    state = Neuron<n>::load(state);
    Neuron<n>::weights.push_back(*state++);
    return state;
}

template<unsigned n>
BiasedNeuron<n>::BiasedNeuron():
    Neuron<n>()
{
    Neuron<n>::weights.push_back(distribution(randomTwister)/std::sqrt(n));
    Neuron<n>::inputs.push_back(1);
    Neuron<n>::learningAlgorithm = Neuron<n>::learningAlgorithm->clone(this);
}

template<unsigned n>
BiasedNeuron<n>::BiasedNeuron(std::vector<ArithmeticType>::const_iterator& state):
    Neuron<n>(state)
{
    Neuron<n>::weights.push_back(*state++);
    Neuron<n>::inputs.push_back(1);
    Neuron<n>::learningAlgorithm = Neuron<n>::learningAlgorithm->clone(this);
}
