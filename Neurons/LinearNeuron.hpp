#pragma once

#include "BiasedNeuron.hpp"

template<unsigned n>
class LinearNeuron : public BiasedNeuron<n>
{
public:
    using BiasedNeuron<n>::BiasedNeuron;
protected:
    virtual ArithmeticType activationFunction() const override;
    virtual ArithmeticType activationFunctionDerivative() const override;
};

template<unsigned n>
ArithmeticType LinearNeuron<n>::activationFunction() const
{
    return Neuron<n>::s;
}

template<unsigned n>
ArithmeticType LinearNeuron<n>::activationFunctionDerivative() const
{
    return 1;
}
