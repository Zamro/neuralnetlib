#pragma once

#include <cmath>

#include "BiasedNeuron.hpp"

template<unsigned n>
class SoftmaxNeuron : public BiasedNeuron<n>
{
public:
    using BiasedNeuron<n>::BiasedNeuron;
    ArithmeticType getExponent() const;
    void setExponentSum(ArithmeticType exponentSum);
protected:
    virtual ArithmeticType activationFunction() const override;
    virtual ArithmeticType activationFunctionDerivative() const override;
    ArithmeticType exponentSum;
};
template<unsigned n>
ArithmeticType SoftmaxNeuron<n>::getExponent() const
{
    return exp(this->s);
}

template<unsigned n>
void SoftmaxNeuron<n>::setExponentSum(ArithmeticType newExponentSum)
{
    exponentSum = newExponentSum;
}

template<unsigned n>
ArithmeticType SoftmaxNeuron<n>::activationFunction() const
{
    return exponentSum ? getExponent()/exponentSum : 0;
}

template<unsigned n>
ArithmeticType SoftmaxNeuron<n>::activationFunctionDerivative() const
{
    auto activation = activationFunction();
    return activation * (1 - activation);
}

