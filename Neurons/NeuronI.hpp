#pragma once

#include <vector>
#include <memory>
#include "../common.hpp"

struct LearningAlgorithm;

class NeuronI
{
    friend LearningAlgorithm;
protected:
    ArithmeticType s{};
    std::unique_ptr<LearningAlgorithm> learningAlgorithm;
    std::vector<ArithmeticType> inputs;
    std::vector<ArithmeticType> weights{};
    virtual ArithmeticType activationFunction() const = 0;
    virtual ArithmeticType activationFunctionDerivative() const = 0;
    virtual ~NeuronI() {}
public:
    NeuronI() = default;
    NeuronI(NeuronI&&) = default;
    ArithmeticType getOutput();
    ArithmeticType learn(ArithmeticType);
    void finishBatch();
    std::vector<ArithmeticType> save() const;
    virtual std::vector<ArithmeticType>::const_iterator load(std::vector<ArithmeticType>::const_iterator state) = 0;

    void setLearningAlgorithm(const LearningAlgorithm& learningAlgorithm);
    const std::vector<ArithmeticType>& getWeights() const;
    ArithmeticType getSumOfSquaredWeights();
};
