#pragma once

#include <array>
#include <cmath>
#include <random>
#include <chrono>
#include <vector>
#include <functional>
#include <memory>

#include "../common.hpp"
#include "../LearningAlgorithms/Backprop.hpp"
#include "NeuronI.hpp"

namespace
{
std::mt19937 randomTwister(std::chrono::system_clock::now().time_since_epoch().count());
std::normal_distribution<ArithmeticType> distribution (0.0, 1);
}


template<unsigned n>
class Neuron: public NeuronI
{
    static_assert(n > 0, "Neuron needs at least one input!");
public:
    Neuron();
    Neuron(std::vector<ArithmeticType>::const_iterator& state);

    void setInput(const std::array<ArithmeticType, n>& input);
    virtual std::vector<ArithmeticType>::const_iterator load(std::vector<ArithmeticType>::const_iterator state) override;
};

template<unsigned n>
Neuron<n>::Neuron()
{
    inputs = std::vector<ArithmeticType>(n,0);
    weights.reserve(n);
    for(unsigned i=0; i<n; i++)
        weights.push_back(distribution(randomTwister)/std::sqrt(n));
    learningAlgorithm = std::unique_ptr<LearningAlgorithm>{new Backprop(this)};
}


template<unsigned n>
Neuron<n>::Neuron(std::vector<ArithmeticType>::const_iterator& state)
{
    inputs = std::vector<ArithmeticType>(n,0);
    state = load(state);
    learningAlgorithm = std::unique_ptr<LearningAlgorithm>{new Backprop(this)};
}

template<unsigned n>
void Neuron<n>::setInput(const std::array<ArithmeticType, n>& inputs)
{
    s = 0;
    for (unsigned i = 0; i < n; i++)
        this->inputs[i] = inputs[i];
    for (unsigned i = 0; i < weights.size(); i++)
        s += weights[i] * this->inputs[i];
}

template<unsigned n>
std::vector<ArithmeticType>::const_iterator Neuron<n>::load(std::vector<ArithmeticType>::const_iterator state)
{
    weights.clear();
    for(unsigned i=0; i<n; i++) {
        weights.push_back(*state++);
    }
    return state;
}
