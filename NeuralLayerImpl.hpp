#pragma once
#include <iostream>
#include <sstream>
#include <memory>
#include <vector>
#include <functional>

#include "common.hpp"
#include "ErrorFunctions.hpp"

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
class NeuralLayerImpl : public NeuralNetI
{
    template<template<unsigned> class, template<unsigned> class, unsigned, unsigned, unsigned...>
    friend class NeuralNet;

    static_assert(inputs > 0, "All layers have to be non empty");
    static_assert(nodesNumber > 0, "All layers have to be non empty");
public:
    typedef std::array<ArithmeticType, nodesNumber> OutputType;
    typedef std::array<ArithmeticType, inputs> InputType;
    typedef std::vector<std::pair<InputType, OutputType>> Data;
private:
    OutputType learn(const OutputType& feedback);
    InputType learnAndGetPreviousLayerDerivatives(const OutputType& errorDerivativesOfLayerOutputs);
    void setInput(const InputType& input);
    virtual void prepareOutput(){};
    OutputType getOutput();
    void finishBatch();

    std::unique_ptr<ErrorFunction<OutputType>> errorFunction{new StandardError<OutputType>()};
protected:
    std::array<std::unique_ptr<NeuronType<inputs>>, nodesNumber> neurons;
public:
    NeuralLayerImpl();
    NeuralLayerImpl(std::vector<ArithmeticType>::const_iterator& state);
    NeuralLayerImpl(const std::vector<ArithmeticType>::const_iterator& state);

    OutputType predict(const InputType& input);
    ArithmeticType train(const InputType& input, OutputType feedback);
    ArithmeticType train(const std::vector<std::pair<InputType, OutputType>>& data);
    template<class Iterator>
    ArithmeticType train(Iterator begin, const Iterator& end);

    std::vector<ArithmeticType> save() const;
    std::string saveToString() const;
    std::vector<ArithmeticType>::const_iterator load(std::vector<ArithmeticType>::const_iterator state);
    void load(const std::string& str);

    void setLearningAlgorithm(const LearningAlgorithm& learningAlgorithm);
    void setErrorFunction(std::unique_ptr<ErrorFunction<OutputType>>&& errorFunction);
    ArithmeticType getSumOfSquaredWeights() const override;
};

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
auto NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    learn(const OutputType& feedback) -> OutputType
{
    OutputType layerInputError;
    for(unsigned i=0;i<nodesNumber;i++)
        layerInputError[i] =  neurons[i]->learn(feedback[i]);
    return layerInputError;
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
auto NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    learnAndGetPreviousLayerDerivatives(const OutputType& errorDerivativesOfLayerOutputs)->InputType
{
    OutputType errorDerivativesOfLayerInputs = learn(errorDerivativesOfLayerOutputs);
    InputType errorDerivativesOfPreviousLayerOutputs{{}};

    for(unsigned i=0; i<nodesNumber; i++)
    {
        const auto& errorDerivativeOfNodeInput = errorDerivativesOfLayerInputs[i];
        const auto& weights = neurons[i]->getWeights();
        for(unsigned j=0; j<inputs; j++)
            errorDerivativesOfPreviousLayerOutputs[j] += errorDerivativeOfNodeInput * weights[j];
    }
    return errorDerivativesOfPreviousLayerOutputs;
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
void NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    setInput(const std::array<ArithmeticType, inputs>& input)
{
    for(auto& neuron: neurons)
        neuron->setInput(input);
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
auto NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    getOutput() -> OutputType
{
    this->prepareOutput();
    OutputType out;
    for(unsigned i=0; i<nodesNumber; i++)
        out[i] = neurons[i]->getOutput();
    return out;
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
void NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    finishBatch()
{
    for(auto& ptr: neurons)
        ptr->finishBatch();
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    NeuralLayerImpl()
{
    for(auto& ptr: neurons)
        ptr = std::unique_ptr<NeuronType<inputs>>(new NeuronType<inputs>());
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    NeuralLayerImpl(std::vector<ArithmeticType>::const_iterator& state)
{
    for(auto& ptr: neurons)
        ptr = std::unique_ptr<NeuronType<inputs>>(new NeuronType<inputs>(state));
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
NeuralLayerImpl(const std::vector<ArithmeticType>::const_iterator& state)
{
    auto stateCopy = state;
    for(auto& ptr: neurons)
        ptr = std::unique_ptr<NeuronType<inputs>>(new NeuronType<inputs>(stateCopy));
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
auto NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    predict(const std::array<ArithmeticType, inputs>& input) -> OutputType
{
    setInput(input);
    return getOutput();
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
ArithmeticType  NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    train(const InputType& input, OutputType feedback)
{
    return train({{input,feedback}});
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
ArithmeticType  NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    train(const std::vector<std::pair<InputType, OutputType> >& data)
{
    return train(data.begin(), data.end());
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
template<class Iterator>
ArithmeticType  NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    train(Iterator begin, const Iterator& end)
{
    ArithmeticType errorOut{};
    unsigned count = 0;
    while(begin != end)
    {
        auto output = predict(begin->first);
        errorOut += errorFunction->error(output, begin->second);
        learn(errorFunction->errorDerivative(output, begin->second));
        ++begin;
        ++count;
    }
    finishBatch();
    return errorOut/count;
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
std::vector<ArithmeticType> NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    save() const
{
    std::vector<ArithmeticType> out;
    out.reserve(inputs*nodesNumber);
    for(const auto& neuron : neurons)
    {
        auto neuronSave = neuron->save();
        out.insert(out.end(), neuronSave.begin(), neuronSave.end());
    }
    return out;
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
std::string NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    saveToString() const
{
    auto out = save();
    return std::string(reinterpret_cast<char*>(&*out.begin()), out.size()*sizeof(ArithmeticType));
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
std::vector<ArithmeticType>::const_iterator NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    load(std::vector<ArithmeticType>::const_iterator state)
{
    for(auto& neuron : neurons)
        state = neuron->load(state);
    return state;
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
void NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    load(const std::string& str)
{
    auto state = convertStringToVector(str);
    load(state.begin());
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
void NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    setLearningAlgorithm(const LearningAlgorithm& learningAlgorithm)
{
    for(auto& ptr: neurons)
        ptr->setLearningAlgorithm(learningAlgorithm);
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
void NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    setErrorFunction(std::unique_ptr<ErrorFunction<OutputType>>&& errorFunction)
{
    this->errorFunction = std::move(errorFunction);
    this->errorFunction->setNeuralNet(this);
}

template <template<unsigned> class NeuronType, unsigned inputs, unsigned nodesNumber>
ArithmeticType NeuralLayerImpl<NeuronType, inputs, nodesNumber>::
    getSumOfSquaredWeights() const
{
    ArithmeticType sum{};
    for(auto& neuron: neurons)
        sum += neuron->getSumOfSquaredWeights();
    return sum;
}
