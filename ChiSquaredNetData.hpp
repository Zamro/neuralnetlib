#pragma once
#include <vector>
#include <utility>

template<class InputType, class OutputType>
struct ChiSquaredNetData {
    std::vector<std::pair<InputType, OutputType>> points;
    std::vector<OutputType> deviations;
};
