#include "Utilities.hpp"
#include <filesystem>

std::string loadNet(const std::string &filename) {
    std::stringbuf buf;
    std::ifstream in(filename, std::ios_base::binary);
    if(not in.is_open()) {
        const auto path = std::filesystem::current_path();
        std::cerr << "Failed opening file: " << path << "\\" << filename;
    }
    else{
        in>>&buf;
        in.close();
    }
    return buf.str();
}

std::ostream &operator<<(std::ostream &out, std::pair<ArithmeticType, ArithmeticType> errors) {
    return out<<errors.first<<" "<<errors.second;
}
